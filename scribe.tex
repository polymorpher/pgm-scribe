\documentclass[twoside]{article}

\usepackage{epsfig}
\usepackage{amsmath,amsfonts,graphicx}

\setlength{\oddsidemargin}{0.25 in}
\setlength{\evensidemargin}{-0.25 in}
\setlength{\topmargin}{-0.6 in}
\setlength{\textwidth}{6.5 in}
\setlength{\textheight}{8.5 in}
\setlength{\headsep}{0.75 in}
\setlength{\parindent}{0 in}
\setlength{\parskip}{0.1 in}

\newcommand{\lecture}[3]{
   \pagestyle{myheadings}
   \thispagestyle{plain}
   \newpage
   \setcounter{page}{1}
   \noindent
   \begin{center}
   \framebox{
      \vbox{\vspace{2mm}
    \hbox to 6.28in { {\bf 10-708:~Probabilistic Graphical Models, Spring 2015 \hfill} }
       \vspace{6mm}
       \hbox to 6.28in { {\Large \hfill #1  \hfill} }
       \vspace{6mm}
       \hbox to 6.28in { {\it Lecturer: #2 \hfill Scribes: #3} }
      \vspace{2mm}}
   }
   \end{center}
   \markboth{#1}{#1}
   \vspace*{4mm}
}

\begin{document}

\lecture{16 : Monte Carlo Methods}{Eric P. Xing}{Aaron Q Li, Jay-Yoon Lee} % Lecture name, Lecturer, Scribes


\section{Representation}


\paragraph*{\textmd{Many distributions can be represented in closed form, example:
\[
f(x)=\frac{1}{(2\pi|\Sigma|)^{1/2}}\exp(-\frac{1}{2}(x-\mu)^{T}\Sigma^{-1}(x-\mu))
\]
}}


\paragraph*{\textmd{And the expectation $E_{p}[f(x)]$ is easy to derive.}}


\paragraph*{\textmd{But there are even more distributions that cannot be expressed
in closed form.}}


\section{Monte Carlo methods}


\paragraph*{\textmd{Concept: Draw random samples from a distribution and compute
marginals and expectations using sample-based average. e.g: 
\[
E[f(x)]=\frac{1}{N}\sum_{i}f(x)^{(i)}
\]
}}


\paragraph*{\textmd{This works with arbitrary model and is asymptotically exact.
However, there are three challenges: }}
\begin{itemize}
\item How to draw samples from a given dist (not all distributions can be trivially sampled).
\item How to make better use of the samples (not all sample are useful, or eqally useful).
\item How to know when we have enough sample for estimation.
% \item It is unclear when we draw enough samples
% \item Doesn't make the best use of each sample
% \item Drawing samples from $f(x)$ can be difficult.
\end{itemize}

\paragraph*{Example\textmd{:}}

\begin{figure}[hbtm]
\centering
\includegraphics[width=0.7\textwidth]{fig/NaiveSample}
\caption{Example of Monte Carlo method: Naive Sampling}
\end{figure}


\paragraph*{\textmd{Naive Sampling: In the lecture slide, there is an example is provided
using samples generated from a Bayesian network where variable values
are discrete. We can constrcut the samples according to probabilities given in a BN by traversing through the tree while using the truth table on each node. The probability estimation can be done using simple frequency
counting. However, problems arise when some value of some variable is not present
in the pool of samples, thus giving unknown (or zero, according to
the frequency counting paradigm) probabilities of some marginal and
conditional distribution.  This implies we need more samples to accurately
estimate a probability. However, in many situations the number of
samples required is exponential in numbers. Furthermore, the naive sampling method would be too costly for high dimensional cases, and therefore we introduce other alternatives from rejection sampling to weighted resampling. }}

% Preview source code from paragraph 13 to 29


\section{Rejection Sampling}


\paragraph*{\textmd{Suppose we want to sample from a distribution $\Pi(x)=\frac{1}{Z}\Pi'(x)$,
where $Z$ is an unknown normaliser. In many situations, $\Pi(x)$
is difficult to sample but $\Pi'(x)$ is easy to evaluate. Rejection
sampling provides a simple procedure to make use of this property
and sample from the original distribution by utilising a simpler distribution,
$Q(x)$. The procedure is as the following:}}

\begin{figure}[hbtm]
\centering
\includegraphics[width=0.4\textwidth]{fig/RejectionSample}
\caption{Illustration of the Rejecton Sampling}
\end{figure}

\paragraph*{\textmd{
\begin{eqnarray*}
\mbox{Draw} &  & x'\sim Q(x)\protect\\
\mbox{Accept with proportion to} &  & \frac{\Pi(x')}{kQ(x')}
\end{eqnarray*}
}}


\paragraph*{\textmd{Where $k$ is a constant chosen in a way so to ensure $kQ(x)\ge\pi(x)$.
The correctness of this procedure can be easily shown by using Bayesian
inference as described below. 
}}

\paragraph*{\textmd{
\begin{eqnarray*}
p(x) & = & \frac{[\Pi'(x)/kQ(x)]Q(x)}{\int[\Pi'(x)/kQ(x)]Q(x)dx}\\
 & = & \frac{\Pi'(x)}{\int\Pi'(x)dx}=\Pi(x).
\end{eqnarray*}
}}


\paragraph*{\textmd{However, as one can visualise, if the proposal distribution
does not match the shape of original distribution closely, this procedure
can yield a high number of rejected samples, thus making it inefficient. }}

\paragraph*{\textmd{Moreover, the value of $k$ has to be determined before the
procedure can start. This is not possible in many situations.}}


\section{Importance Sampling}


\paragraph*{\textmd{Instead of formulating a distribution with a density function
everywhere greater or equal to original distribution, and subsequently
rejecting samples, importance sampling uses a weighting scheme on
the generated samples. Suppose we generate $M$ samples from the proposal
distribution $q$, and assume $\pi(x^{(t)})$ can be evaluated (}note\textmd{:
this is non-trivial assumption). Then the mean of arbitrary function
$f(x)$ can be computed by 
\[
E[f(x)]=\frac{1}{M}\sum_{i=1}^{M}f(x^{(i)})w^{(i)}
\]
}}


\paragraph*{\textmd{Where $w^{(t)}=\frac{\pi(x^{(t)})}{q(x^{(t)})}$ and $x^{(t)}$
are the samples from $q$. }}


\paragraph*{\textmd{In the case which $\pi(x)$ cannot be evaluated directly.
Assume $p'(x)=\alpha p(x)$, where $\alpha$ is an unknown normaliser.
Let $r(x)=\frac{\pi'(x)}{q(x)}$, it follows that 
\[
E_{q}[r(x)]=\int\frac{\pi'(x)}{q(x)}q(x)dx=\alpha
\]
}}


\paragraph*{\textmd{Now it can be shown that (see slides) 
\begin{eqnarray*}
E_{p}[f(x)] & = & \frac{1}{M}\sum_{i=1}^{M}f(x^{(i)})r(x^{(t)})/\sum_{i}r(x^{(t)})
\end{eqnarray*}
}}


\paragraph*{\textmd{In other words, the weights $\tilde{w}^{(t)}$ is now determined
by $r(x^{(t)})/\sum_{i}r(x^{(t)})$, and $E[f(x)]=\frac{1}{M}\sum_{i=1}^{M}f(x^{(i)})\tilde{w}^{(i)}$.}}


\section{Weighted Resampling}


\paragraph*{\textmd{Note even though importance sampling resolves one issue in
rejection sample, that the value of $k$ has to be determined a priori,
the efficiency of the procudure still relies on how well the proposal
distribution $q$ matches the original distribution $\pi$. To see
this, suppose the original distribution has a substantial mass concenrtrated
in a small region, and the proposal distribution does not. Following
the procedure of importance sampling, it is not hard to see that samples
generated in this region is going to have a much higher weight than
samples in other regions. Thus, the estimated result is likely to
contain large error.}}


\paragraph*{\textmd{There are two solutions to this issue. One is to use a heavy
tail proposal distribution, the other is to resample from the samples
according to the weights. However, this means a high number of samples
is required to give reasonable estimate. The textbook (Mackay 29.2)
shows an example which the weight difference could result requiring
exponential number of samples in order to give any reasonable estimate
of $E_{p}[f(x)]$.}}


% Preview source code from paragraph 28 to 33


\section{Particle Filter}


\paragraph*{\textmd{Particle Filter is a tool to estimate the hidden state $x_{t}$
(at time $t$) of a hidden markov model with known transition probability
$\pi(x_{t+1}|x_{t})$, and the expectation value over a function at
state $x_{t}$, approximated by 
\[
\int f(x_{t})p(x_{t}|y_{0:t})dx\approx\frac{1}{N}\sum_{i=1}^{N}f(x_{t}^{(i)})
\]
}}


\paragraph*{\textmd{Where $y_{0:t}$ are the observations from time $0$ to $t$,
and $x_{t}^{(i)}$ with $i=1,...,N$ are the samples. Depending on
the proposal distribution, there are many ways to sample each $x_{t}$
and update the weights (see Wikipedia, for example). The one introduced
in class uses the following mechanism: following the same derivation
in importance sampling, it can be shown that the quantity $p(x_{t}|y_{1:t})$
can be represented by 
\begin{eqnarray*}
x_{t}^{(m)} & \sim & p(x_{t}|y_{1:t-1})\protect\\
w_{t}^{(m)} & = & \frac{p(y_{t}|x_{t}^{(m)})}{\sum_{m}p(y_{t}|x_{t}^{(m)})}
\end{eqnarray*}
}}


\paragraph*{\textmd{Where $m=1,...,M$ are the samples and $w_{t}^{(m)}$ are
the weights. We draw $x_{t+1}$ according to the following distribution:
\begin{eqnarray*}
p(x_{t+1}|y_{1:t}) & = & \int p(x_{t+1}|x_{t})p(x_{t}|y_{1:t})dx_{t}\protect\\
 & = & \sum_{m=1}^{M}w_{t}^{(i)}p(x_{t+1}|x_{t}^{(m)})
\end{eqnarray*}
}}


\paragraph*{\textmd{And repeat the previous process of drawing $M$ samples for
timestamp $t+1$. i.e }}


\paragraph*{\textmd{
\begin{eqnarray*}
x_{t+1}^{(m)} & \sim & p(x_{t+1}|y_{1:t})\protect\\
w_{t+1}^{(m)} & = & \frac{p(y_{t+1}|x_{t+1}^{(m)})}{\sum_{m}p(y_{t+1}|x_{t+1}^{(m)})}
\end{eqnarray*}
}}

\paragraph*{\textmd{(To be continued)}}


\end{document}

